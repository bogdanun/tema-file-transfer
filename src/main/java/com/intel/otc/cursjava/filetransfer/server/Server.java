package com.intel.otc.cursjava.filetransfer.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.intel.otc.cursjava.filetransfer.common.CommonUtils;
import com.intel.otc.cursjava.filetransfer.common.Config;

public class Server implements Runnable {

	private Config config;
	private ServerSocket serverSocket;
	private boolean isStopped = false;

	public Server(Config config) {
		this.config = config;
	}

	public void run() {
		openServerSocket();
		while (!this.isStopped()) {
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				if (isStopped()) {
					System.out.println("Server Stopped.");
					return;
				}
				throw new RuntimeException("Error accepting client connection", e);
			}
			new Thread(new ConnectionHandler(clientSocket, config.getServerDownloadDir(),
					config.getByteBufferSizeForTransfer())).start();
		}
	}

	public synchronized boolean isStopped() {
		return isStopped;
	}

	public synchronized void stop() {
		this.isStopped = true;
		CommonUtils.closeStreams(serverSocket);
	}

	public void openServerSocket() {
		try {
			serverSocket = new ServerSocket(config.getPort());
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot open server on port " + config.getPort());
		}
	}

}
