package com.intel.otc.cursjava.filetransfer;

import com.intel.otc.cursjava.filetransfer.client.Client;
import com.intel.otc.cursjava.filetransfer.common.Config;
import com.intel.otc.cursjava.filetransfer.common.Config.NodeType;
import com.intel.otc.cursjava.filetransfer.server.Server;

public class App {
	public static void main(String[] args) throws Exception {
		Config config = new Config(args);
		if (config.getNodeType() == NodeType.SERVER) {
			new Thread(new Server(config)).start();
		} else if (config.getNodeType() == NodeType.CLIENT) {
			new Thread(new Client(config)).start();
		}
	}
}
