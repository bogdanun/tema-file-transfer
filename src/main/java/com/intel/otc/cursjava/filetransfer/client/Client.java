package com.intel.otc.cursjava.filetransfer.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.intel.otc.cursjava.filetransfer.common.CommonUtils;
import com.intel.otc.cursjava.filetransfer.common.Config;

public class Client implements Runnable {

	private Config config;

	public Client(Config config) {
		this.config = config;
	}

	public void run() {
		Socket socket = null;
		PrintWriter socketOut = null;
		BufferedReader fileReader = null;
		try {
			socket = new Socket(config.getHostName(), config.getPort());
			socketOut = new PrintWriter(socket.getOutputStream(), true);
			Path fileForTransfer = Paths.get(config.getClientUploadFile());
			fileReader = Files.newBufferedReader(fileForTransfer);

			char[] buffer = new char[config.getByteBufferSizeForTransfer()];

			socketOut.write(fileForTransfer.getFileName().toString() + "\n");
			socketOut.flush();
			while (fileReader.read(buffer) > 0) {
				socketOut.write(buffer);
				socketOut.flush();
			}
		} catch (IOException e) {
			throw new RuntimeException("Error communicating with server", e);
		} finally {
			CommonUtils.closeStreams(socket, socketOut, fileReader);
		}
	}
}
