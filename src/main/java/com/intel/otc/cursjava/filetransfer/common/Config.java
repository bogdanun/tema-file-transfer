package com.intel.otc.cursjava.filetransfer.common;

public class Config {
	public enum NodeType {
		CLIENT, SERVER
	};

	private String hostName;
	private NodeType nodeType;
	private int port;
	private String serverDownloadDir;
	private String clientUploadFile;
	private int byteBufferSizeForTransfer = 1024;

	public int getByteBufferSizeForTransfer() {
		return byteBufferSizeForTransfer;
	}

	public String getHostName() {
		return hostName;
	}

	public String getServerDownloadDir() {
		return serverDownloadDir;
	}

	public String getClientUploadFile() {
		return clientUploadFile;
	}

	public int getPort() {
		return port;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public Config(NodeType nodeType, String hostName, int port) {
		this.nodeType = nodeType;
		this.hostName = hostName;
		this.port = port;
	}

	public Config(String[] args) throws Exception {

		int portArgsIndex = 0;

		try {
			if (args[0].contains("client")) {
				nodeType = NodeType.CLIENT;
				portArgsIndex = 2;
			} else if (args[0].contains("server")) {
				nodeType = NodeType.SERVER;
				portArgsIndex = 1;
			}
		} catch (Exception e) {
			throw new Exception("please specify server/client as first command line argument");
		}

		if (nodeType == NodeType.CLIENT && args.length < 4) {
			throw new Exception(
					"error while starting file transfer: specify 'client hostname port filepath' as command line arguments ");
		} else if (nodeType == NodeType.SERVER && args.length < 3) {
			throw new Exception(
					"error while starting file transfer: specify 'server port dirpath' as command line arguments ");
		}

		if (nodeType == NodeType.CLIENT) {
			hostName = args[1];
			clientUploadFile = args[3];
		} else {
			serverDownloadDir = args[2];
		}

		try {
			port = Integer.parseInt(args[portArgsIndex]);
		} catch (Exception e) {
			System.out.println("please specify a valid port number as third command line argument");
			throw e;
		}
	}
}
