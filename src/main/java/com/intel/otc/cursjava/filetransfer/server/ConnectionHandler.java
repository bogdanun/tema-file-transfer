package com.intel.otc.cursjava.filetransfer.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.intel.otc.cursjava.filetransfer.common.CommonUtils;

public class ConnectionHandler implements Runnable {
	private Socket clientSocket;
	private String downloadDir;
	private int bufferSize;

	public ConnectionHandler(Socket clientSocket, String downloadDir, int bufferSize) {
		this.downloadDir = downloadDir;
		this.clientSocket = clientSocket;
		this.bufferSize = bufferSize;
	}

	public void run() {
		BufferedReader socketReader = null;
		try {
			System.out.println("connection established");
			socketReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); 
			String incomingFileName = socketReader.readLine();
			System.out.println("Incoming file:" + incomingFileName);
			// TO DO - handle duplicate file names from different clients 
			writeIncomingFile(incomingFileName, socketReader);
		} catch (IOException e) {
			System.out.println("Error while handling connection.");
			e.printStackTrace();
		} finally {
			CommonUtils.closeStreams(socketReader, clientSocket);
		}
	}

	public void writeIncomingFile(String fileName, BufferedReader socketReader) throws IOException {
		Charset charset = Charset.forName("US-ASCII");
		Path file = Paths.get(downloadDir, fileName);
		System.out.println("Writing file: " + file.toString());
		BufferedWriter fileWriter = Files.newBufferedWriter(file, charset);
		char[] buffer = new char[bufferSize];
		while (socketReader.read(buffer) > 0) {
			fileWriter.write(buffer);
		}
		fileWriter.flush();
		fileWriter.close();
	}
}
